package pro.eugw.NJoin;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        saveDefaultConfig();
        toConsole("Enabled");
        scheduleTask();
    }

    @Override
    public void onDisable() {
        toConsole("Disabled");
    }

    private static void toConsole(String msg) {
        Bukkit.getConsoleSender().sendMessage("[Join] " + msg);
    }

    private static void toPlayer(String msg, CommandSender commandSender) {
        commandSender.sendMessage("[Join] " + msg);
    }

    private void scheduleTask() {
        getServer().getScheduler().runTaskTimer(this, () -> {
            java.util.Calendar c = java.util.Calendar.getInstance();
            SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss");
            String t = f.format(c.getTime());
            String[] times = t.split(":");
            int hour = Integer.parseInt(times[0]);
            int minute = Integer.parseInt(times[1]);
            int sec = Integer.parseInt(times[2]);
            if (hour == 23 && minute == 59 && sec == 59) {
                File log = new File(getDataFolder(), "join.log");
                File logDir = new File(getDataFolder(), "logs");
                if (!logDir.exists()) {
                    logDir.mkdir();
                }
                Date now = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM");
                File newFile = new File(logDir, simpleDateFormat.format(now) + ".log");
                log.renameTo(newFile);
            }
        }, 0, 20);
    }
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        String cmdn = command.getName();
        if (cmdn.equals("njoin")) {
            if (!commandSender.hasPermission("njoin.reload")) {
                toPlayer(getConfig().getString("msg.noPerm").replace("&", "\u00a7"), commandSender);
                return false;
            }
            reloadConfig();
            toPlayer(getConfig().getString("msg.reload").replace("&", "\u00a7"), commandSender);
            return true;
        }
        return false;
    }
    private void log(String msg) throws Exception {
        if (!getConfig().getBoolean("settings.log")){
            return;
        }
        File logFile = new File(getDataFolder(), "join.log");
        if (!logFile.exists()) {
            logFile.createNewFile();
        }
        FileWriter fileWriter = new FileWriter(logFile, true);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println(msg);
        printWriter.close();
    }

    @EventHandler
    public final void onJoin(PlayerJoinEvent event) throws Exception {
        if (!getConfig().getBoolean("settings.join")){
            return;
        }
        Date n = new Date();
        SimpleDateFormat f = new SimpleDateFormat("dd-MM HH:mm:ss");
        Player player = event.getPlayer();
        if (!player.hasPermission("njoin.bc.join")) {
            return;
        }
        String name = player.getName();
        String prefix = PermissionsEx.getPermissionManager().getUser(player).getPrefix();
        String msg = getConfig().getString("msg.join").replace("%prefix%", prefix).replace("%name%", name).replace("&", "\u00a7");
        log("[" + f.format(n) + "]" + " Player " + name + " " + prefix.replaceAll("(&([a-z0-9]))", "") + " with IP: " + player.getAddress().toString().replace("/", ""));
        getServer().broadcastMessage(msg);
    }

    @EventHandler
    public final void onQuit(PlayerQuitEvent event) {
        if (!getConfig().getBoolean("settings.quit")){
            return;
        }
        Player player = event.getPlayer();
        if (!player.hasPermission("njoin.bc.quit")) {
            return;
        }
        String name = player.getName();
        String prefix = PermissionsEx.getPermissionManager().getUser(player).getPrefix();
        String msg = getConfig().getString("msg.quit").replace("%prefix%", prefix).replace("%name%", name).replace("&", "\u00a7");
        getServer().broadcastMessage(msg);
    }
}